$(function(){
    var shrinkHeader = 400;
    $(window).scroll(function() {
        var scroll = getCurrentScroll();
        if ( scroll >= shrinkHeader ) {
            $('.nav-header').addClass('shrink');
            $('.navigation').addClass('shrink');
            $('.buttonresponsive').addClass('shrink');
        }
        else {
            $('.nav-header').removeClass('shrink');
            $('.navigation').removeClass('shrink');
            $('.buttonresponsive').removeClass('shrink');
        }
    });
    function getCurrentScroll() {
        return window.pageYOffset || document.documentElement.scrollTop;
    }
});
$(document).ready(function(){
   $('.leader-list').slick({
       infinite: true,
       slidesToShow: 5,
       slidesToScroll: 1,
       responsive: [
           {
               breakpoint: 992,
               settings: {
                   slidesToShow: 4,
               }
           },
           {
               breakpoint: 840,
               settings: {
                   slidesToShow: 3,
               }
           },
           {
               breakpoint: 600,
               settings: {
                   slidesToShow: 2,
                   slidesToScroll: 2,
               }
           },
           {
               breakpoint: 480,
               settings: {
                   slidesToShow: 1,
                   slidesToScroll: 1,
               }
           }
       ],
       prevArrow: '.prev',
       nextArrow: '.next'
   });

    $('.client-testi').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 5000,
        dots: true
    });

    var modalVerticalCenterClass = ".modal";
    function centerModals($element) {
        var $modals;
        if ($element.length) {
            $modals = $element;
        } else {
            $modals = $(modalVerticalCenterClass + ':visible');
        }
        $modals.each( function(i) {
            var $clone = $(this).clone().css('display', 'block').appendTo('body');
            var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
            top = top > 0 ? top : 0;
            $clone.remove();
            $(this).find('.modal-content').css("margin-top", top);
        });
    }
    $(modalVerticalCenterClass).on('show.bs.modal', function(e) {
        centerModals($(this));
    });
    $(window).on('resize', centerModals);

    $("#openresponsive").click(function(){
        $(".overlay").fadeToggle(200);
       $(this).toggleClass('btn-open').toggleClass('btn-close');
    });

    $('.overlay').on('click', function(){
        $(".overlay").fadeToggle(200);
        $(".button a").toggleClass('btn-open').toggleClass('btn-close');
        open = false;
    });

    $('.core-slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 5000,
        asNavFor: '.core-nav'
    });
    $('.core-nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.core-slider',
        dots: false,
        centerMode: true,
        focusOnSelect: true
    });

    $('.services-desc').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 5000,
        asNavFor: '.services-nav'
    });

    $('.services-nav').slick({
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 5000,
        asNavFor: '.services-desc',
        centerMode: true,
        focusOnSelect: true
    });

    $('.barometer-slider').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 5000,
        dots: true,
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ],
    });
});
